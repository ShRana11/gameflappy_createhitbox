package com.example.sukhrana.flappybird;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Player {
    Bitmap Image;
    Rect Hitbox;
    private  int xPosition;
    private  int yPosition;

    public Player(Context context,  int x, int  y ){
        this.xPosition = x;
        this.yPosition = y;

        //set default image- allenemy have samne image and hitbox
        this.Image = BitmapFactory.decodeResource(context.getResources(), R.drawable.alien_ship1);

        // version 1 - we use static numbers because the enemy is NOT moving.
        this.Hitbox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + this.Image.getWidth(),
                this.yPosition + this.Image.getHeight()
        );
    }

    public Bitmap getImage() {
        return Image;
    }

    public void setImage(Bitmap image) {
        Image = image;
    }

    public Rect getHitbox() {
        return Hitbox;
    }

    public void setHitbox(Rect hitbox) {
        Hitbox = hitbox;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }
}
